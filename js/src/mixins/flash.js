import swal from 'sweetalert2';

export default {

    methods: {
        dialog(message, type, title, miliseconds) {
            let options = {
                type: type,
                title: title,
                text: message
            };

            if(miliseconds)
                options['timer'] = miliseconds;

            return swal(options);
        },

        flashSuccess(message, title = "", timer = 2000) {
            return this.dialog(message, 'success', title, timer);
        },

        flashError(message, title = "", timer = 2000) {
            return this.dialog(message, 'error', title, timer);
        },

        flashInfo(message, title = "", timer = 2000) {
            return this.dialog(message, 'info', title, timer);
        },

        alertSuccess(message, title = "") {
            return this.dialog(message, 'success', title, null);
        },

        alertError(message, title = "") {
            return this.dialog(message, 'error', title, null);
        },

        alertInfo(message, title = "") {
            return this.dialog(message, 'info', title, null);
        },

        confirm(message, title = 'Confirm', okText = 'Aceptar', cancelText = 'Cancelar', okColor = '#3085d6', cancelColor = '#888') {

            let options = {
                type: 'warning',
                title: title,
                text: message,
                showCancelButton: true,
                allowOutsideClick: false,
                confirmButtonText: okText,
                cancelButtonText: cancelText,
                confirmButtonColor: okColor,
                cancelButtonColor: cancelColor
            };

            return swal(options);
        },

        inputBox(title, value, inputType = 'text', okText = 'Guardar') {
            let oldValue = value;

            let self = this;

            let options = {
                type: 'question',
                input: inputType,
                title: title,
                inputValue: value,
                timer: null,
                showCancelButton: true,
                confirmButtonText: okText,
                showLoaderOnConfirm: true,
                allowOutsideClick: false,
                preConfirm: function (value) {
                    return new Promise( function (resolve, reject) {
                        if(value == oldValue) {
                            reject('Nada para actualizar');
                        }else{
                            resolve();
                        }
                    });
                }
            };

            return swal(options);
        }
    }
}
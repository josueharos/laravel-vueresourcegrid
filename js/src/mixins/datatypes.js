import moment from 'moment';
import accounting from 'accounting';

export default {
    methods: {
        formatDateDMY(value) {
            return moment(value).format('DD/MM/YYYY');
        },
        formatGender(value) {
            let genderColor = value == 'M' ? 'primary' : 'danger';
            let genderIcon = value == 'M' ? 'mars' : 'venus';

            return '<span class="label label-' + genderColor + '"><i class="fa fa-' + genderIcon + '"></i>&nbsp;' + value + '</span>';
        }
    }
}
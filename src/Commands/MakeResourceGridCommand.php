<?php

namespace JHTech\VueResourceGrid\Commands;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Config;
use Symfony\Component\Process\Process;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Process\Exception\ProcessFailedException;

class MakeResourceGridCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'grid:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a new grid';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'GridDefinition';

    /**
     * Singular name for the model
     *
     * @var string
     */
    protected $singular;

    /**
     * Plural name for the model
     *
     * @var string
     */
    protected $plural;

    /**
     * Hyphen case
     *
     * @var string
     */
    protected $singularVarName;

    /**
     * Hyphen case
     *
     * @var string
     */
    protected $pluralVarName;

    /**
     * Information to build Grid
     *
     * @var array
     */
    protected $grid = [];

    /**
     * Information to build Controller
     *
     * @var array
     */
    protected $controller = [];

    /**
     * Information to build Request
     *
     * @var array
     */
    protected $request = [];

    /**
     * Information for the model
     *
     * @var array
     */
    protected $model = [];

    /**
     * Information for the views
     *
     * @var array
     */
    protected $views = [];

    /**
     * Information for Vue components
     *
     * @var array
     */
    protected $vue = [];

    /**
     * Stores the classname
     *
     * @var string
     */
    protected $className = '';

    /**
     * View Prefix Blade Syntax
     * @var string
     */
    protected $viewPrefix = '';

    /**
     * Command options
     *
     * @var array
     */
    protected $options = [
        ['plural', 'p', InputOption::VALUE_REQUIRED, 'Plural', null]
    ];

    protected $arguments = [];

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__ . '/../stubs/grid.stub';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getRequestStub()
    {
        return __DIR__ . '/../stubs/request.stub';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getControllerStub()
    {
        return __DIR__ . '/../stubs/controller.stub';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getFormStub()
    {
        return __DIR__ . '/../stubs/views/form.stub';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getEditStub()
    {
        return __DIR__ . '/../stubs/views/edit.stub';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getCreateStub()
    {
        return __DIR__ . '/../stubs/views/create.stub';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getIndexStub()
    {
        return __DIR__ . '/../stubs/views/index.stub';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getShowStub()
    {
        return __DIR__ . '/../stubs/views/show.stub';
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getDetailRowStub()
    {
        return __DIR__ . '/../stubs/vue/detailrow.stub';
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {

        return $this->options;
    }


// **********************************************************************************************
// **********************************************************************************************
// **********************************************************************************************
// **********************************************************************************************
    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceModelName($stub, $name)
    {

        return str_replace('DummyModel', $name, $stub);
    }

    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceOnStub($stub, $replace, $search)
    {

        return str_replace($search, $replace, $stub);
    }

    /**
     * Get singular and plural for the model
     *
     * @method buildSingularPlural
     *
     * @return Self
     */
    protected function buildSingularPlural()
    {
        $this->singular = ucfirst($this->getNameInput());

        $this->plural = ($this->option('plural')!== null) ? $this->option('plural') : Str::plural($this->singular);

        $this->singularVarName = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $this->singular));

        $this->pluralVarName = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $this->plural));

        return $this;
    }

    /**
     * Parses name
     *
     * @param  string $name
     * @return string
     */
    protected function parseGridName($name, $namespace)
    {
        if (Str::startsWith($name, $namespace)) {
            return $name;
        }

        if (Str::contains($name, '/')) {
            $name = str_replace('/', '\\', $name);
        }

        return $this->parseGridName($this->getDefaultNamespace(trim($namespace, '\\')) . '\\' . $name, $namespace);
    }

    /**
     * Build information for the grid
     *
     * @method buildGridClassName
     *
     * @return Self
     */
    protected function buildGridClassName()
    {
        $this->grid['namespace'] = config("vueresourcegrid.namespace.grid");

        $this->grid['class'] = $this->parseGridName($this->singular, $this->grid['namespace']) . 'Grid';

        $this->grid['path'] = $this->getPath($this->grid['class']);

        return $this;
    }

    /**
     * Build controller class name
     *
     * @method buildControllerClassName
     *
     * @return Self
     */
    protected function buildControllerClassName()
    {
        $this->controller['namespace'] = config("vueresourcegrid.namespace.controller");

        $this->controller['class'] = $this->parseGridName($this->plural, $this->controller['namespace']) . 'Controller';

        $this->controller['path'] = $this->getPath($this->controller['class']);

        return $this;
    }

    /**
     * Builds request information
     *
     * @method buildRequestClassName
     *
     * @return Self
     */
    protected function buildRequestClassName()
    {
        $this->request['namespace'] = config("vueresourcegrid.namespace.request");

        $this->request['class'] = $this->parseGridName($this->singular, $this->request['namespace']) . 'FormRequest';

        $this->request['path'] = $this->getPath($this->request['class']);

        return $this;
    }

    /**
     * Builds model information
     *
     * @method buildModelClassName
     *
     * @return Self
     */
    protected function buildModelClassName()
    {
        $this->model['namespace'] = config("vueresourcegrid.namespace.model");

        $this->model['name'] = $this->model['namespace'] . '\\' . $this->singular;

        return $this;
    }

    /**
     * Build options for views
     *
     * @method buildViewOptions
     *
     * @return Self
     */
    protected function buildViewOptions()
    {
        $this->views['root'] = resource_path('views') . DIRECTORY_SEPARATOR . config('vueresourcegrid.views.path');

        $this->views['path'] = $this->views['root'] . DIRECTORY_SEPARATOR . strtolower($this->pluralVarName);

        return $this;
    }

    /**
     * Build vue options
     *
     * @method buildVueOptions
     *
     * @return Self
     */
    protected function buildVueOptions()
    {
        $this->vue['path'] = resource_path('assets/js/vendor/jhtech/vueresourcegrid/');

        $this->vue['detailrows_path'] = $this->vue['path'] . 'components/DetailRows/';

        $this->vue['detailrow_file'] = $this->vue['detailrows_path'] . '/Grid' . $this->singular . 'Row.vue';

        return $this;
    }

    /**
     * Check if files exist
     *
     * @method classAlreadyExists
     *
     * @return Boolean
     */
    protected function classAlreadyExists()
    {
        if($this->files->exists($this->grid['path'])) {
            $this->error('Grid Class already exists!');
            return true;
        }
        if($this->files->exists($this->controller['path'])) {
            $this->error('Controller Class already exists!');
            return true;
        }
        if($this->files->exists($this->request['path'])) {
            $this->error('Request Class already exists!');
            return true;
        }
    }

    /**
     * Build paths if they do not exist
     *
     * @method buildPaths
     *
     * @return Self
     */
    protected function buildPaths()
    {
        $this->makeDirectory($this->grid['path']);
        $this->makeDirectory($this->controller['path']);
        $this->makeDirectory($this->request['path']);
        $this->makeDirectory($this->views['path'] . '/dummy.php');

        return $this;
    }

    /**
     * Build the gridclass
     *
     * @method buildGridClass
     *
     * @return string
     */
    public function buildGridClass()
    {
        $stub = $this->files->get($this->getStub());

        $class = $this->replaceNamespace($stub, $this->grid['class'])
                        ->replaceClass($stub, $this->grid['class']);

        $class = $this->replaceOnStub($class, strtolower($this->pluralVarName), 'DummyResource');

        $this->info('Added grid class.');

        return $class;
    }

    /**
     * Builds the controller class
     *
     * @method buildControllerClass
     *
     * @return string
     */
    protected function buildControllerClass()
    {
        $stub = $this->files->get($this->getControllerStub());

        $class = $this->replaceNamespace($stub, $this->controller['class'])
                        ->replaceClass($stub, $this->controller['class']);

        $class = $this->replaceModelName($class, $this->model['name']);

        $class = $this->replaceOnStub($class, $this->grid['class'], 'DummyGridClass');
        $class = $this->replaceOnStub($class, config("vueresourcegrid.views.path"), 'DummyViewPath');
        $class = $this->replaceOnStub($class, strtolower($this->pluralVarName), 'DummyResource');
        $class = $this->replaceOnStub($class, strtolower($this->singularVarName), 'DummyViewVarName');
        $class = $this->replaceOnStub($class, $this->request['class'], 'DummyRequestClass');

        $this->info('Added resource contoller.');

        return $class;
    }

    /**
     * Build the request class
     *
     * @method buildRequestClass
     *
     * @return string
     */
    public function buildRequestClass()
    {
        $stub = $this->files->get($this->getRequestStub());

        $class = $this->replaceNamespace($stub, $this->request['class'])
                        ->replaceClass($stub, $this->request['class']);

        $class = $this->replaceModelName($class, $this->model['name']);

        $this->info('Added model form request.');

        return $class;
    }

    /**
     * Build the skeleton for the form view
     *
     * @method buildViewForm
     *
     * @return string
     */
    protected function buildViewForm()
    {
        $stub = $this->files->get($this->getFormStub());

        $class = $this->replaceOnStub($stub, strtolower($this->pluralVarName), 'DummyResourceName');
        $class = $this->replaceOnStub($class, strtolower($this->singularVarName), 'DummyViewVarName');
        $class = $this->replaceOnStub($class, strtolower($this->singularVarName), 'DummyModel');

        $this->info('Form blade view added.');

        return $class;
    }

    /**
     * Build edit view
     *
     * @method buildViewEdit
     *
     * @return string
     */
    protected function buildViewEdit()
    {
        $stub = $this->files->get($this->getEditStub());

        $class = $this->replaceOnStub($stub, strtolower($this->pluralVarName), 'DummyResourceName');

        $class = $this->replaceOnStub($class, strtolower($this->singularVarName), 'DummyViewVarName');

        $this->info('Edit blade view added.');

        return $class;
    }

    /**
     * Build create view
     *
     * @method buildViewCreate
     *
     * @return string
     */
    protected function buildViewCreate()
    {
        $stub = $this->files->get($this->getCreateStub());

        $class = $this->replaceOnStub($stub, strtolower($this->pluralVarName), 'DummyResourceName');

        $class = $this->replaceOnStub($class, strtolower($this->singularVarName), 'DummyViewVarName');

        $this->info('Create blade view added.');

        return $class;
    }

    /**
     * Build index view
     *
     * @method buildViewIndex
     *
     * @return string
     */
    protected function buildViewIndex()
    {
        $stub = $this->files->get($this->getIndexStub());

        $class = $this->replaceOnStub($stub, strtolower($this->singularVarName), 'DummyViewVarName');

        $this->info('Index blade view added.');

        return $class;
    }

    /**
     * Build show view
     *
     * @method buildViewShow
     *
     * @return string
     */
    protected function buildViewShow()
    {
        $stub = $this->files->get($this->getShowStub());

        $class = $this->replaceOnStub($stub, strtolower($this->singularVarName), 'DummyViewVarName');

        $class = $this->replaceOnStub($class, strtolower($this->pluralVarName), 'DummyResourceName');

        $this->info('Show blade view added.');

        return $class;
    }

    /**
     * Creates the model and the migration
     *
     * @method createModel
     *
     * @return Self
     */
    protected function createModel()
    {
        $process = new Process("php artisan make:model {$this->singular} -m ", base_path());

        $process->run();

        if(! $process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $this->info($process->getOutput());

        return $this;
    }

    /**
     * Add resource route to the routes files in
     * routes/web.php L5.3+
     *
     * @method addResourceRoute
     */
    protected function addResourceRoute()
    {
        $routesFilePath = base_path('routes/web.php');

        $controllerName = $this->plural . 'Controller';

        $resourceRoute = PHP_EOL . "Route::resource('/{$this->pluralVarName}','{$controllerName}');" . PHP_EOL;

        $this->files->append($routesFilePath, $resourceRoute);

        $this->info('Added resource route.');

        return $this;
    }

    /**
     * Insert component in js
     *
     * @method addVueComponentToJs
     */
    protected function addVueComponentToJs()
    {
        $componentsFile = $this->vue['path'] . 'detailrows.js';

        $component = PHP_EOL . "Vue.component('grid-" . strtolower($this->singularVarName) . "-row',require('./components/DetailRows/Grid" .$this->singular . "Row.vue'));" . PHP_EOL;

        $this->files->append($componentsFile, $component);

        $this->info('Component added to js');

        return $this;
    }

    /**
     * Build a detail row example component
     *
     * @method buildDetailRowComponent
     *
     * @return Self
     */
    protected function buildDetailRowComponent()
    {
        $stub = $this->files->get($this->getDetailRowStub());

        $class = $this->replaceOnStub($stub, strtolower($this->singularVarName), 'DummyViewVarName');

        $this->info('Vue detail row added.');

        return $class;
    }

    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function handle()
    {
        $this->buildSingularPlural()
                ->buildGridClassName()
                ->buildControllerClassName()
                ->buildRequestClassName()
                ->buildModelClassName()
                ->buildViewOptions()
                ->buildVueOptions();

        if($this->classAlreadyExists())
            return false;
        $this->buildPaths();

        $this->createModel();


        $this->files->put($this->grid['path'], $this->buildGridClass());

        $this->files->put($this->request['path'], $this->buildRequestClass());

        $this->files->put($this->controller['path'], $this->buildControllerClass());

        $this->files->put($this->views['path'] . '/_form.blade.php', $this->buildViewForm());

        $this->files->put($this->views['path'] . '/edit.blade.php', $this->buildViewEdit());

        $this->files->put($this->views['path'] . '/create.blade.php', $this->buildViewCreate());

        $this->files->put($this->views['path'] . '/show.blade.php', $this->buildViewShow());

        $this->files->put($this->views['path'] . '/index.blade.php', $this->buildViewIndex());

        $this->files->put($this->vue['detailrow_file'], $this->buildDetailRowComponent());

        $this->addVueComponentToJs();

        $this->addResourceRoute();

        $this->info($this->type . ' Done.');
    }
}

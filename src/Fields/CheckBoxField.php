<?php

namespace JHTech\VueResourceGrid\Fields;

use JHTech\VueResourceGrid\FieldDefinition;

class CheckBoxField extends FieldDefinition
{
    protected $name = '__checkbox';

    protected $titleClass = 'text-center';

    protected $dataClass = 'text-center';
}
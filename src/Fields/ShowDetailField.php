<?php

namespace JHTech\VueResourceGrid\Fields;

use JHTech\VueResourceGrid\FieldDefinition;

class ShowDetailField extends FieldDefinition
{
    protected $name = '__slot:detail';

    protected $title = ' ';

    protected $dataClass = 'text-center';

    protected $titleClass = 'text-center';

    protected $sortable = false;
}
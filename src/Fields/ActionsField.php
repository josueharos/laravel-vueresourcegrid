<?php

namespace JHTech\VueResourceGrid\Fields;

use JHTech\VueResourceGrid\FieldDefinition;

class ActionsField extends FieldDefinition
{
    protected $name = '__slot:actions';

    protected $title = 'Acciones';

    protected $dataClass = 'text-center';

    protected $titleClass = 'text-center';

    protected $sortable = false;
}
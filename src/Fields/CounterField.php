<?php

namespace JHTech\VueResourceGrid\Fields;

use JHTech\VueResourceGrid\FieldDefinition;

class CounterField extends FieldDefinition
{
    protected $name = '__sequence';

    protected $title = '#';

    protected $titleClass = 'text-center';

    protected $dataClass = 'link-button';
}
<?php

namespace JHTech\VueResourceGrid\Fields;

use JHTech\VueResourceGrid\FieldDefinition;

class DateField extends FieldDefinition
{
    protected $formatCallback = 'formatDateDMY';
}
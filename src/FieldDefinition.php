<?php

namespace JHTech\VueResourceGrid;

abstract class FieldDefinition
{   
    /**
     * Field name
     *
     * @var string
     */
    protected $name;

    /**
     * Field title
     *
     * @var string
     */
    protected $title;

    /**
     * Title css class
     *
     * @var string
     */
    protected $titleClass;

    /**
     * Data css class
     *
     * @var string
     */
    protected $dataClass;

    /**
     * Field name that responds to sort, if sortable
     *
     * @var string
     */
    protected $sortField;
    
    /**
     * Check if field is sortable
     *
     * @var boolean
     */
    protected $sortable = true;
    
    /**
     * Format callback
     *
     * @var string
     */
    protected $formatCallback = '';

    /**
     * Set the visibility of the field
     *
     * @var boolean
     */
    protected $visible = true;

    /**
     * Editable attribute
     *
     * @var boolean
     */
    protected $editable = true;

    /**
     * Creates and sanitizes the array
     *
     * @method toArray
     *
     * @return array
     */
    public function toArray()
    {
        $values = [];

        $values['name'] = $this->getName();
        
        $values['title'] = $this->getTitle();

        if($this->isSortable())
            $values['sortField'] = $this->getSortedField();

        if($this->hasCallback())
            $values['callback'] = $this->formatCallback;

        if($this->hasTitleClass())
            $values['titleClass'] = $this->titleClass;

        if($this->hasDataClass())
            $values['dataClass'] = $this->dataClass;

        if(! $this->isVisible())
            $values['visible'] = $this->visible;

        // if($this->isReadOnly())
            $values['editable'] = $this->editable;

        return $values;
    }

    /**
     * Converts to json
     *
     * @method toJson
     *
     * @return json
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }

    /**
     * Checks if field is sortable
     *
     * @method isSortable
     *
     * @return boolean
     */
    public function isSortable()
    {
        return $this->sortable;
    }
        
    /**
     * Checks if field is visible
     *
     * @method isVisible
     *
     * @return boolean
     */
    public function isVisible()
    {
        return $this->visible;
    }

    /**
     * Checks if field is editable
     *
     * @method isEditable
     *
     * @return boolean
     */
    public function isEditable()
    {
        return $this->editable;
    }

    /**
     * Checks if field has a title set
     *
     * @method hasTitleClass
     *
     * @return boolean
     */
    public function hasTitleClass()
    {
        return (! is_null($this->titleClass) && $this->titleClass != '');
    }

    /**
     * Checks if field has a data class set
     *
     * @method hasDataClass
     *
     * @return boolean
     */
    public function hasDataClass()
    {
        return (! is_null($this->dataClass) && $this->dataClass != '');
    }

    /**
     * Checks if a title has been set
     *
     * @method hasTitle
     *
     * @return boolean
     */
    public function hasTitle()
    {
        return (! is_null($this->title) && $this->title != '');
    }

    /**
     * Checks if field has a callback set
     *
     * @method hasCallback
     *
     * @return boolean
     */
    public function hasCallback()
    {
        return (! is_null($this->formatCallback) && $this->formatCallback != '');
    }

    /**
     * Get name
     *
     * @method getName
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get or make title
     *
     * @method getTitle
     *
     * @return string
     */
    public function getTitle()
    {
        return ($this->hasTitle()) ? $this->title : ucwords($this->name);
    }

    /**
     * Get title class
     *
     * @method getTitleClass
     *
     * @return string
     */
    public function getTitleClass()
    {
        return $this->titleClass;
    }

    /**
     * Get data class
     *
     * @method getDataClass
     *
     * @return string
     */
    public function getDataClass()
    {
        return $this->dataClass;
    }

    /**
     * Get sort fieldname
     *
     * @method getSortedField
     *
     * @return string
     */
    public function getSortedField()
    {
        return ($this->sortField == '') ? $this->name : $this->sortField;
    }

    /**
     * Set the sort option off
     *
     * @method notSortable
     *
     * @return Self
     */
    public function notSortable()
    {
        $this->sortable = false;    
    
        return $this;
    }

    /**
     * Sets the name attribute
     *
     * @method setName
     *
     * @param  string  $value
     */
    public function withName($value)
    {
        $this->name = $value;
    
        return $this;
    }

    /**
     * Sets the format callback
     *
     * @method setCallback
     *
     * @param  string      $value
     */
    public function withCallback($value)
    {
        $this->formatCallback = $value;
    
        return $this;
    }

    /**
     * Set the title attribute
     *
     * @method setTitle
     *
     * @param  string   $value
     */
    public function withTitle($value)
    {
        $this->title = $value;
    
        return $this;
    }

    /**
     * Set the dataClass attribute
     *
     * @method setDataClass
     *
     * @param  string  $value
     */
    public function withDataClass($value)
    {
        $this->dataClass = $value;
    
        return $this;
    }

    /**
     * Set the titleClass attribute
     *
     * @method setTitleClass
     *
     * @param  string        $value
     */
    public function withTitleClass($value)
    {
        $this->titleClass = $value;
    
        return $this;
    }

    /**
     * Sets sortable and the sort field
     *
     * @method setSortField
     *
     * @param  string       $value
     */
    public function setSortField($value)
    {
        $this->sortable = true;

        $this->sortField = $value;
    
        return $this;
    }

    /**
     * Hides the field
     *
     * @method hide
     *
     * @return Self
     */
    public function hide()
    {
        $this->visible = false;

        return $this;
    }

    /**
     * Sets the field as readonly
     *
     * @method readOnly
     *
     * @return Self
     */
    public function readOnly()
    {
        $this->editable = false;
    
        return $this;
    }

    /**
     * Attach class to hide on small screen
     *
     * @method hideOnSmallScreen
     *
     * @return self
     */
    public function hideOnSmallScreen()
    {
        $this->titleClass .= ' hidden-xs ';
        $this->dataClass .= ' hidden-xs ';
    
        return $this;
    }
    
    /**
     * Hide on normal screen
     *
     * @method hideOnNormalScreen
     *
     * @return Self
     */
    public function hideOnNormalScreen()    
    {
        $this->titleClass .= ' hidden-sm hidden-md hidden-lg ';
        $this->dataClass .= ' hidden-sm hidden-md hidden-lg ';
        
    
        return $this;
    }

    /**
     * Attach class to hide on small screen
     *
     * @method hideOnSmallScreen
     *
     * @return self
     */
    public function showOnSmallScreen()
    {
        $this->titleClass .= ' visible-xs ';
        $this->dataClass .= ' visible-xs ';
    
        return $this;

    
        return $this;
    }

    /**
     * Static method
     *
     * @return 
     */
    public static function make()
    {
        return new static;
    }
}
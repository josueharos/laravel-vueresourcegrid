<?php

namespace JHTech\VueResourceGrid\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BaseFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // to be implemented
        return true;
    }

    /**
     * Add the id of the current model to the unique validation
     * to avoid exceptions
     *
     * @method attachIdExceptionOnUpdateUnique
     *
     * @return array
     */
    protected function attachIdExceptionOnUpdateUnique()
    {
        if($this->isUpdating())
        {
            $applicableRules = [];
            
            foreach ($this->rules as $field => $rules) {
                $rules = array_map(function($rule) use($field) {
                    if(strpos($rule, 'unique') !== 0) 
                        return $rule;

                    $segment = $this->segments();
                    
                    $instance = ($this->model)::find(end($segment));

                    return "{$rule},{$instance->id}";

                }, explode('|', $rules));             
               
                $applicableRules[$field] = implode('|', $rules);
            }
            
            return $applicableRules;
        }
        
        return $this->rules;
    }

    /**
     * Resolve rules
     *
     * @method resolveRules
     *
     * @return array
     */
    protected function resolveRules()
    {
        if(! $this->wantsJson()) 
            return $this->attachIdExceptionOnUpdateUnique();

        return array_intersect_key($this->rules, $this->all());
    }

    /**
     * Detects the request type
     *
     * @return boolean
     */
    protected function isUpdating()
    {

        return (strtolower($this->method()) == 'put' || strtolower($this->method()) == 'patch');
    }

    /**
     * Creates the model
     *
     * @method create
     *
     * @param  Eloquent\Model $model
     *
     * @return Eloquent\Model
     */
    protected function new()
    {
        $class = $this->model;

        return $class::create($this->all());
    }

    /**
     * Updates the model
     *
     * @method update
     *
     * @param  Eloquent\Model $model
     *
     * @return Eloquent\Model
     */
    protected function update($model)
    {
        
        return $model->update($this->all());
    }

    /**
     * Persists the request to DB
     *
     * @method save
     *
     * @param  boolean $createMode 
     * @param  Eloquent\Model  $model
     *
     * @return Request
     */
    public function save($createMode = true, $model = null)
    {   
        if ($createMode) 
            return $this->new();

        return $this->update($model);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return $this->resolveRules();
    }    
}
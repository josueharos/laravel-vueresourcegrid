<?php

namespace JHTech\VueResourceGrid\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GridRequest extends FormRequest
{
    /**
     * Flag to inspect if request has sort information
     *
     * @var boolean
     */
    public $hasSort = false;
    
    /**
     * Flag to inspectif request has pagination
     *
     * @var boolean
     */
    public $hasPagination = true;

    /**
     * Contains the sort fieldname, if any
     *
     * @var string
     */
    public $sortField;

    /**
     * Sort direction
     *
     * @var string
     */
    protected $sortDirection = 'asc';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */    
    public function authorize()
    {
        return true;
    }

    /**
     * Sets value for the request
     *
     * @method inspectRequest
     *
     * @return $this
     */
    protected function inspectRequest()
    {
        $this->hasPagination = $this->has('per_page') || $this->has('page');

        $this->sortField = $this->has('sort') ? $this->sort : 'id';

        $this->hasSort = $this->sortField <> '';

        $this->sortDirection = 'asc';

        if(strrpos($this->sortField, '|'))
        {
            $values = explode('|', $this->sortField);

            $this->sortField = $values[0];

            $this->sortDirection = $values[1];
        }

        return $this;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->inspectRequest();

        return [];
    }

    /**
     * Build paginator with appends
     *
     * @method buildPaginator
     *
     * @param  Builder         $builder
     * @param  string         $model
     *
     * @return Paginator
     */
    protected function buildPaginator($builder, $model)
    {
        $appends = [];
        
        if (( new $model ) instanceof SearchableModelContract) 
            $appends = $instance->getSearchAppendFields();

        $paginator = $builder->paginate($this->per_page)->toArray();
        
        $data = $model::hydrate($paginator['data']);
        
        $hydrated = [];

        foreach ($data as $item) {
            array_push($hydrated, $item->append($appends)->toArray());
        }

        $paginator['data'] = $hydrated;

        return $paginator;
    }

   /**
     * Paginates the request
     *
     * @method paginateRequest
     *
     * @param  \Illuminate\Database\Eloquent\QueryBuilder         $model
     *
     * @return Illuminate\Support\Collection
     */
    public function paginateRequest($builder, $model = '')
    {   
        if ($this->hasSort) 
            $builder = $builder->orderBy($this->sortField, $this->sortDirection);
        
        if($this->hasPagination)
            return $this->buildPaginator($builder, $model);
        
        return $builder->get()->toArray();
    }   
}
<?php

namespace JHTech\VueResourceGrid\Traits;

trait HandlesResponse
{
    /**
     * Generate response
     *
     * @method generateResponse
     *
     * @param  Boolean           $condition
     * @param  string            $redirect
     *
     * @return Json/Redirect
     */
    protected function generateResponse($condition, $redirect)
    {
        if(request()->wantsJson())
            return $this->generateJsonResponse($condition);

        return $this->generateRedirect($condition, $redirect);
    }

    /**
     * Generate Json Response
     *
     * @method generateJsonResponse
     *
     * @param  Boolean               $condition
     *
     * @return Json
     */
    protected function generateJsonResponse($condition)
    {
        if($condition)
            return response()->json(['status' => 200,'data' => $condition], 200);

        return response()->json(['status' => 405, 'data' => $condition], 405);
    }

    /**
     * Generate redirect response
     *
     * @method generateRedirect
     *
     * @param  Boolean           $condition
     * @param  string           $redirect
     *
     * @return Redirect
     */
    protected function generateRedirect($condition, $redirect)
    {
        if($condition)
            return redirect($redirect);

        return abort('404', 'Error');
    }
}
<?php

namespace JHTech\VueResourceGrid\Search;

use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use JHTech\VueResourceGrid\Contracts\SearchableModelContract;

trait SearchableTrait
{
    /**
     * Searchable fields
     *
     * @var array
     */
    protected $searchableFields = [];

    /**
     * Stores the query string
     *
     * @var string
     */
    protected $queryString;

    /**
     * Get append attributes
     *
     * @method getSearchAppendFields
     *
     * @return array
     */
    public function getSearchAppendFields()
    {

        return array_merge($this->appends,['searchTitle', 'searchSubtitle']);
    }

    /**
     * Gets searchable fields, if defined
     *
     * @method getSearchableFields
     *
     * @return array
     */
    public function getSearchableFields()
    {
        if(property_exists($this, 'searchOn'))
            return $this->searchOn;

        if ($this->fillable != [])
            return $this->fillable;

        return ['*'];
    }

    /**
     * Checks if model is searchable
     *
     * @method isSearchable
     *
     * @return boolean
     */
    public static function isSearchable()
    {

        return (new static) instanceof SearchableModelContract;
    }

    /**
     * Simple search on model
     *
     * @method simpleSearch
     *
     * @return Json/Array
     */
    public function simpleSearch($relations = null)
    {
        // Get class name
        $class = get_class();

        // Get search fields
        $searchFields = $this->getSearchableFields();

        // Get appends
        $appends = $this->getSearchAppendFields();

        // Get the empty builder
        $builder = $this->getQuery();

        $filter = (is_null($this->queryString)) ? $this['queryString'] : $this->queryString;

        foreach ($searchFields as $field)
            $builder->orWhere($field, 'like', $filter);

        // Bring results
        $result = $builder->get();

        // Convert to models
        if (! is_null($relations) ) {
            $result = $class::hydrate($result->toArray())->load($relations);
        }else{
            $result = $class::hydrate($result->toArray());
        }

        // return $result->each->append($appends)->toArray();
        $resultArray = [];

        foreach ($result as $item) {
            $arrayItem = $item->append($appends)->toArray();
            $arrayItem['search_related'] = false;
            $arrayItem['search_query'] = '';
            $resultArray[] = $arrayItem;
        }

        return $resultArray;
    }

    /**
     * Search on relations
     *
     * @method searchOnRelation
     *
     * @param  mixed           $value
     * @param  array           $relations
     *
     * @return Json/Array
     */
    public function searchOnRelation($keyValue, $relations)
    {
        $primaryResults = $this->simpleSearch($relations);

        $instance = $this;

        $filter = (is_null($this->queryString)) ? $this['queryString'] : $this->queryString;

        foreach ($relations as $relation) {
            $searchModel = $instance->$relation()->getRelated();

            $class = get_class($searchModel);

            $searchFields = $searchModel->getSearchableFields();

            $appends = $searchModel->getSearchAppendFields();

            // Get the empty builder
            $builder = $searchModel->getQuery();

            $builder->where(function($q) use ($filter, $searchFields) {
                foreach ($searchFields as $field) {
                    $q->orWhere($field, 'like', $filter);
                }
            });

            // Bring results
            $builderResults = $builder->get();

            // Convert to models
            $modelResults = $class::hydrate($builderResults->toArray());

            $rawResults = new Collection;

            foreach ($modelResults as $model) {
                if (! method_exists($model, $keyValue)) {
                    $keyValue = Str::singular(str_replace('_', '', ucwords($keyValue,'_')));
                }

                $rawResults->push($model->$keyValue);
            }
        }
        $result = [];
        if (count($rawResults) > 0) {
            $resultArray = [];
            foreach ($rawResults as $item) {
                $arrayItem = $item->append($appends)->toArray();
                $arrayItem['search_related'] = true;
                $arrayItem['search_query'] = $this->queryString;
                $resultArray[] = $arrayItem;
            }
            $rawResults = $resultArray;

        }else{
            $rawResults = [];
        }

        $rawResults =  array_merge($rawResults, $primaryResults);

        $duplicatedIds = [];

        foreach ($rawResults as $item) {
            if (!in_array($item['id'], $duplicatedIds)) {
                $result[] = $item;
                array_push($duplicatedIds, $item['id']);
            }
        }


        return $result;
    }

    /**
     * Static method
     *
     * @return
     */
    public static function search($queryString, $keyValue = null, $relations = [])
    {
        $instance = new static;

        $instance->queryString = $queryString;

        if(count($relations) > 0 && ! is_null($keyValue))
            return $instance->searchOnRelation($keyValue, $relations);

        return $instance->simpleSearch();
    }
}
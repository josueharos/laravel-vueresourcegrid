<?php

namespace JHTech\VueResourceGrid\Search;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    /**
     * Default namespace for the models
     *
     * @var string
     */
    protected $baseModelNamespace = '\\App\\';

    /**
     * Request query field name
     *
     * @var string
     */
    protected $defaultQueryField = 'q';

    /**
     * Name of the model
     *
     * @var string
     */
    protected $searchModel;

    /**
     * Model relations
     *
     * @var array
     */
    protected $models = [];

    /**
     * Model relations
     *
     * @var array
     */
    protected $relations = [];

    /**
     * Transform the name of the model
     *
     * @method getModelName
     *
     * @param  model       $model
     *
     * @return string
     */
    protected function getModelName($model)
    {

        return $this->baseModelNamespace . Str::singular(str_replace('_', '', ucwords($model,'_')));
    }

    /**
     * Validates the request
     *
     * @method isRequestValid
     *
     * @return boolean
     */
    protected function isRequestValid()
    {

        return class_exists($this->searchModel) ||
               in_array('App\SearchableTrait',class_uses($this->searchModel)) ||
               request()->has($this->defaultQueryField);
    }

    /**
     * Formats the query value
     *
     * @method formatQueryValue
     *
     * @return string
     */
    protected function formatQueryValue()
    {
        return '%' . request($this->defaultQueryField) . '%';
    }

   /**
     * Validates the model
     *
     * @method validateModel
     *
     * @param  string        $model
     *
     * @return self
     */
    protected function validateModel($model)
    {
        $modelName = $this->getModelName($model);

        $this->models = [$modelName => $model];

        $this->searchModel = $modelName;

        return $this;
    }

    /**
     * Strips all prefixes before search
     *
     * @method stripUrlSegments
     *
     * @param  array           $segments
     *
     * @return array
     */
    protected function stripUrlSegments($segments)
    {
        $found = false;

        foreach ($segments as $segment)
        {
            if ($segment !== 'search' && !$found)
            {
                array_shift($segments);
            }
            elseif($segment == 'search')
            {
                array_shift($segments);
                $found = true;
            }
        }

        return $segments;
    }

    /**
     * Decode the search for simple relation
     *
     * @method decodeSearchRequest
     *
     * @return self
     */
    protected function decodeSearchRequest()
    {
        // Get url segments
        $segments = request()->segments();

        // Take out /**/search
        $segments = $this->stripUrlSegments($segments);

        if (count($segments) > 1)
        {

            $models = [];
            $values = [];

            foreach ($segments as $segment) {
                if(preg_match('/^[0-9]+$/', $segment) == 1) {
                    array_push($values, $segment);
                } else {
                    array_push($models, $segment);
                }
            }

            $this->relations[] = array_pop($models);

            $this->models = array_merge($models, $values);

            $this->searchModel = $this->models[0];

            return $this;
        }

        $this->searchModel = end($segments);

        return $this;
    }

    /**
     * Handles search
     *
     * @method index
     *
     * @param  string $model
     *
     * @return Json
     */
    public function index($model)
    {
        $this->decodeSearchRequest();

        $this->validateModel($this->searchModel);

        if(! $this->isRequestValid() )
            return abort(400, 'Bad Request');

        return $this->execute($this->searchModel);
    }

    /**
     * Executes the search
     *
     * @method execute
     *
     * @param  Class  $model
     *
     * @return Array/Json
     */
    protected function execute($model)
    {
        if (! $model::isSearchable())
            return "Model is not searchable";

        if(count($model) > 0 )
            return $model::search($this->formatQueryValue(), $this->models[$model] ,$this->relations);
        return $model::search($this->formatQueryValue());
    }
}

<?php

namespace App\Search\Contracts;

interface RequestTransformer
{
    public function transform($source);
}
<?php

namespace JHTech\VueResourceGrid\Contracts;

interface SearchableModelContract
{
    /**
     * Get caption attribute
     *
     * @method getCaptionAttribute
     *
     * @return string
     */
    public function getCaptionAttribute();
    
    /**
     * Title attribute
     *
     * @method getSearchTitleAttribute
     *
     * @return string
     */
    public function getSearchTitleAttribute();
    
    /**
     * Subtitle attribute
     *
     * @method getSearchSubtitleAttribute
     *
     * @return string
     */
    public function getSearchSubtitleAttribute();
}
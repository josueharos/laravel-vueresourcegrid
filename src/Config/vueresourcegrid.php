<?php

return [
    'namespace' => [
        'grid'       => 'App\Grids',
        'request'    => 'App\Http\Requests',
        'controller' => 'App\Http\Controllers',
        'model'      => 'App',
    ],
    'views'     => [
        'path' => 'pages',
    ],
];

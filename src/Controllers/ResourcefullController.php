<?php

namespace JHTech\VueResourceGrid\Controllers;

use App\Http\Controllers\Controller;
use JHTech\VueResourceGrid\Requests\GridRequest;
use JHTech\VueResourceGrid\Traits\HandlesResponse;

abstract class ResourcefullController extends Controller
{
    use HandlesResponse;

    /**
     * Model of the controller
     *
     * @var string
     */
    protected $model;

    /**
     * Grid for the controller
     *
     * @var string
     */
    protected $grid;

    /**
     * View for the index
     *
     * @var string
     */
    protected $viewPath;

    /**
     * resultUrl
     *
     * @var string
     */
    protected $resultUrl;

    /**
     * View var name
     *
     * @var string
     */
    protected $viewVarName;

    /**
     * Request form
     *
     * @var string
     */
    protected $requestClass;

    /**
     * Display a listing of the resource.
     *
     * @param  GridRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(GridRequest $request)
    {
        if (request()->wantsJson())
            return $request->paginateRequest(($this->model)::getQuery(), $this->model);

        $gridClass = $this->grid;

        $grid = new $gridClass;

        return view($this->viewPath . ".index", compact('grid'));
    }

    /**
     * Display the specified resource.
     *
     * @param  mixed  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $class = $this->model;

        $entity = $class::findOrFail($id);

        if (request()->wantsJson()) {
            return $entity;
        }

        return view($this->viewPath . '.show')
                ->with($this->viewVarName, $entity);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view($this->viewPath . '.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  mixed  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $class = $this->model;

        $entity = $class::findOrFail($id);

        return view($this->viewPath . '.edit')
                ->with($this->viewVarName, $entity);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $form = app($this->requestClass);

        $result = $form->save();

        return $this->generateResponse($result, $this->resultUrl);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  mixed  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $form = app($this->requestClass);

        $class = $this->model;

        $entity = $class::findOrFail($id);

        return $this->generateResponse($form->save(false, $entity), $this->resultUrl);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  mixed  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $class = $this->model;

        $entity = $class::findOrFail($id);

        return $this->generateResponse($entity->delete(), $this->resultUrl);
    }
}
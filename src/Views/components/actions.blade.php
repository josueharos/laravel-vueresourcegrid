@component('components.link', [
    'location'  =>  "/{$resource}/{$id}",
    'size'      =>  'xs',
    'color'     =>  'info',
    'icon'      =>  'eye'
])
@endcomponent
&nbsp;
@component('components.link', [
    'location'  =>  "/{$resource}/{$id}/edit",
    'size'      =>  'xs',
    'color'     =>  'warning',
    'icon'      =>  'pencil'
])
@endcomponent
&nbsp;

<form class="form-inline" 
    action="/{{$resource}}/{{$id}}" 
    method="POST" 
    role="form" 
    style="display: inline-block;">
    
    {{ csrf_field() }}

    {{ method_field('DELETE') }}
    
    @component('components.button', [
        'type'  =>  "submit",
        'size'      =>  'xs',
        'color'     =>  'danger',
        'icon'      =>  'close'
    ])
    @endcomponent
</form>
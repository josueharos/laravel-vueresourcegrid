<a href="{{ $location }}"
        {{ isset($id) ? ' id="' . $id . '" ' : ' ' }}
        class="btn btn-{{ isset($size) ? $size : 'md' }} btn-{{ isset($color) ? $color : 'default' }}">
    <span class="fa fa-{{ isset($icon) ? $icon : 'close' }}"></span>&nbsp;
   {{ isset($caption) ? " {$caption}" : '' }}
</a>
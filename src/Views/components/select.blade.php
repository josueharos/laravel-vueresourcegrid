<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
    <label for="{{ $name }}">
        {{ isset($caption) ? $caption : ucfirst($name) }} <sup class="text-danger">{{isset($required) ? (($required) ? '(*)' : '') : '(*)'}}</sup> :
    </label>
    
    <select 
           name="{{ $name }}"  
           id="{{ $name }}" 
           placeholder="{{ isset($placeholder) ? $placeholder : ''}}" 
           class="form-control" 
           {{isset($required) ? (($required) ? ' required ' : '') : ' required '}}>
                <option value="">{{ isset($empty_option) ? $empty_option : 'Pick one ...' }}</option>
           @foreach($options as $key => $title)
                <option value="{{$key}}" {{ ($value == $key) ? ' selected ' : '' }}>{{$title}}</option>
           @endforeach
    </select>

    @if($errors->has($name))
         <span class="small text-danger"> * {{ $errors->first($name) }}</span>
    @endif   
</div>
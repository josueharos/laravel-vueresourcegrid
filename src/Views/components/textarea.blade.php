<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
    <label for="{{ $name }}">
        {{ isset($caption) ? $caption : ucfirst($name) }} <sup class="text-danger">{{isset($required) ? (($required) ? '(*)' : '') : '(*)'}}</sup>:
    </label>
    
    <textarea 
        name="{{ $name }}"  
        id="{{ $name }}" 
        placeholder="{{ isset($placeholder) ? $placeholder : ''}}" 
        class="form-control" 
        {{isset($required) ? (($required) ? ' required ' : '') : ' required '}}
        cols="{{isset($cols) ? $cols : 30 }}" 
        rows="{{isset($rows) ? $rows : 5 }}"
    >{{ isset($value) && trim($value) != '' ? $value : old($name) }}</textarea>

    @if($errors->has($name))
         <span class="small text-danger"> * {{ $errors->first($name) }}</span>
    @endif   
</div>
<button type="{{ isset($type) ? $type : 'button' }}"
        {{ isset($id) ? ' id="' . $id . '" ' : ' ' }}
        class="btn btn-{{ isset($size) ? $size : 'md' }} btn-{{ isset($color) ? $color : 'primary' }}">
    <span class="fa fa-{{ isset($icon) ? $icon : 'link' }}"></span>&nbsp;
   {{ isset($caption) ? "{$caption}" : '' }}
</button>
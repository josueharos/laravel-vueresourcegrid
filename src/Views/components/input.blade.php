<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
    <label for="{{ $name }}">
        {{ isset($caption) ? $caption : ucfirst($name) }} <sup class="text-danger">{{isset($required) ? (($required) ? '(*)' : '') : '(*)'}}</sup>:
    </label>
    
    <input type="{{ isset($type) ? $type : 'text' }}" 
           name="{{ $name }}"  
           id="{{ $name }}" 
           placeholder="{{ isset($placeholder) ? $placeholder : ''}}" 
           class="form-control" 
           {{isset($required) ? (($required) ? ' required ' : '') : ' required '}}
           value="{{ isset($value) && trim($value) != '' ? $value : old($name) }}"
    />
    
    @if($errors->has($name))
         <span class="small text-danger"> * {{ $errors->first($name) }}</span>
    @endif   
</div>
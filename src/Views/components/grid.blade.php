<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr>
                <th colspan="{{ isset($colspan) ? $colspan : 1 }}" class="text-right">
                    {{ isset($create) ? $create : '' }}
                </th>
            </tr>
            <tr>
                {{ isset($headers) ? $headers : '' }}
            </tr>
        </thead>
        <tbody>
            {{ isset($body) ? $body : '' }}            
        </tbody>
        <tfoot>
            <tr>
                <th colspan="{{ isset($colspan) ? $colspan : 1 }}" class="text-center">
                    {{ isset($footer) ? $footer : '' }}
                </th>
            </tr>
        </tfoot>
    </table>
</div>
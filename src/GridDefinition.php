<?php

namespace JHTech\VueResourceGrid;

abstract class GridDefinition implements \IteratorAggregate
{
    /**
     * Api url
     *
     * @var string
     */
    public $apiUrl;
    
    /**
     * Set fields
     *
     * @var array
     */
    protected $fields;

    /**
     * View
     *
     * @var boolean
     */
    protected $view = true;
    
    /**
     * Edit
     *
     * @var boolean
     */
    protected $edit = false;
    
    /**
     * Delete
     *
     * @var boolean
     */
    protected $delete = false;

    /**
     * Search flag
     *
     * @var boolean
     */
    protected $searchable = true;

    /**
     * Constructor.
     *
     * @method __construct
     */
    public function __construct()
    {
        $this->fields = $this->define();

        $this->apiUrl = $this->setUrl();

        $this->authorize();
    }

    /**
     * Get Iterator
     *
     * @method getIterator
     *
     * @return ArrayIterator
     */
    public function getIterator()
    {

        return new ArrayIterator($this->fields);
    }

    /**
     * Fields to array
     *
     * @method fieldsToArray
     *
     * @return array
     */
    public function fieldsToArray()
    {   
        $values = [];
        
        foreach ($this->fields as $fieldDefinition) 
            array_push($values, $fieldDefinition->toArray());
        
        return $values;
    }

    /**
     * Fields to Json
     *
     * @method fieldsToJson
     *
     * @return json
     */
    public function fieldsToJson()
    {
        return json_encode($this->fieldsToArray());
    }

    /**
     * Authorize actions
     *
     * @method authorize
     *
     * @return Self
     */
    public function authorize()
    {
        return $this;
    }

    /**
     * Checks if can Delete
     *
     * @method canDelete
     *
     * @return Boolean
     */
    public function canDelete()
    {
        
        return boolval($this->delete) + 0;
    }

    /**
     * Checks if can show
     *
     * @method canView
     *
     * @return Boolean
     */
    public function canView()
    {
        
        return boolval($this->view) + 0;
    }

    /**
     * Checks if can edit
     *
     * @method canEdit
     *
     * @return Boolean
     */
    public function canEdit()
    {
        
        return boolval($this->edit) + 0;
    }

    /**
     * Sets the url
     *
     * @method changeUrl
     *
     * @param  string    $url
     *
     * @return Self
     */
    public function changeUrl($url)
    {
        $this->apiUrl = $url;
    
        return $this;
    }

    /**
     * Search off
     *
     * @method searchOff
     *
     * @return Self
     */
    public function searchOff()
    {
        $this->searchable = false;
    
        return $this;
    }

    /**
     * Search on
     *
     * @method searchOn
     *
     * @return Self
     */
    public function searchOn()
    {
        $this->searchable = true;
    
        return $this;
    }

    /**
     * Checks if grid can be searched
     *
     * @method isSearchable
     *
     * @return boolean
     */
    public function isSearchable()
    {
        
        return boolval($this->searchable) + 0;
    }

    /**
     * Define the fields
     *
     * @method define
     *
     * @return Self
     */
    abstract public function define();

    /**
     * Set the url endpoint
     *
     * @method setUrl
     */
    abstract public function setUrl();

}
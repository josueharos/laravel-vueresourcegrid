<?php

namespace JHTech\VueResourceGrid;

use Illuminate\Support\ServiceProvider;
use JHTech\VueResourceGrid\Commands\MakeResourceGridCommand;

class VueResourceGridServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/Routes/routes.php');

        $this->publishes([
            __DIR__ . '/../js/src/' => resource_path('assets/js/vendor/jhtech/vueresourcegrid/')
        ],'jhtech/vueresourcegrid');

        $this->publishes([
            __DIR__ . '/../js/src/components/' => resource_path('assets/js/vendor/jhtech/vueresourcegrid/components/')
        ],'jhtech/vueresourcegrid');

        $this->publishes([
            __DIR__ . '/../js/src/components/DetailRows/' => resource_path('assets/js/vendor/jhtech/vueresourcegrid/components/DetailRows/')
        ],'jhtech/vueresourcegrid');

        $this->publishes([
            __DIR__ . '/../js/src/components/Vuetable/' => resource_path('assets/js/vendor/jhtech/vueresourcegrid/components/Vuetable/')
        ],'jhtech/vueresourcegrid');

        $this->publishes([
            __DIR__ . '/../js/src/mixins/' => resource_path('assets/js/vendor/jhtech/vueresourcegrid/mixins/')
        ],'jhtech/vueresourcegrid');

        $this->publishes([
            __DIR__ . '/Config/vueresourcegrid.php' => config_path('vueresourcegrid.php'),
        ], 'jhtech/vueresourcegrid');

        if ($this->app->runningInConsole()) {
            $this->commands([
                MakeResourceGridCommand::class,
            ]);
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        // $this->registerFacades();

        // Create the controller
        $this->app->make('JHTech\VueResourceGrid\Search\SearchController');

        // Views
        $this->loadViewsFrom(__DIR__ . '/Views/components', 'gridcomponents');

        // Config
        $this->mergeConfigFrom(__DIR__ . '/Config/vueresourcegrid.php', 'vueresourcegrid');
    }
}
# jhtech/vueresourcegrid

Laravel package for creating Grids

Based on:

- ratiw/vuetable-2

It creates generator for:

 - ModelRequest
 - ResourceModelController
 - Grid Generator

## Install

Require this package in your composer.json and update composer.

``` php
"jhtech\vueresourcegrid":"dev-master"
```

Via Composer

``` bash
$ composer require jhtech/vueresourcegrid
```

After updating composer, add the ServiceProvider to the providers array in config/app.php

``` php
JHTech\VueResourceGrid\VueResourceGridServiceProvider::class,
```

Publish configuration and views by running command

``` bash
php artisan vendor:publish --provider="JHTech\VueResourceGrid\VueResourceGridServiceProvider"
```

After that install node dependencies for the Vue part

``` bash
$ npm install

$ npm install accounting moment font-awesome sweetalert2 vuetable-2 vue-typeahead babel-plugin-transform-runtime babel-preset-es2015 babel-preset-stage-2 --save-dev

```
Edit your app.js in assets to import the vue components

``` javascript

require('./vendor/jhtech/vueresourcegrid/grid.js');

```

Import your css assets
``` 
// Font Awesome
@import "node_modules/font-awesome/scss/font-awesome";

// Sweetalert
@import "node_modules/sweetalert2/src/sweetalert2.scss";
```

Compile your assets, and make sure they are loaded in the view. 

``` bash
$ npm run dev
```

If you want, you can run the authentication scaffolding, this will make the layouts.

``` bash
$ php artisan make:auth
```

## Usage

``` bash
php artisan grid:create Model --plural Models
```

###### Options

- --plural,-p: Plural name for the model


##### Search Module

To implement search, make sure you use SearchableTrait in your Model and implement SearchableModelContract on the Model too.
    Also, by default your searchable fields are what you define either on your '$fillable' property or attach the property '$searchOn' and add always the caption to the appends atribute



Example
``` php
namespace App;

use Illuminate\Database\Eloquent\Model;
use JHTech\VueResourceGrid\Search\SearchableTrait;
use JHTech\VueResourceGrid\Contracts\SearchableModelContract;

class Person extends Model implements SearchableModelContract
{
	use SearchableTrait;
	 
    /**
    * ..... Your model code .....
    */
    protected $appends = ['caption'];
    
    protected $searchOn = ['name','email'];
    
    public function getSearchTitleAttribute()
    {
    	return $this->name;
    }
    
    public function getSearchSubtitleAttribute()
    {
    	return $this->email;
    }
    
    public function getCaptionAttribute()
    {
    	return $this->name;
    }
}


```

##### Creating Grid

In your grid class there are 3 methods, is already populated by the generator

- setUrl

	Here you define your api endpoint, to load data from.

``` php
public function setUrl()
{
	return '/resource';
}
```


- define

	Define the fields fot the Grid, as an array

Example:
``` php
public function define()
{
 	return [
      CounterField::make(),

	  DateField()::make()
      	->withName('date'),

      TextField::make()
          ->withName('name')
          ->withTitle('Your name')
          ->withDataClass('text-center')
          ->readOnly()
          ->hideOnSmallScreen(),

  	  ActionsField::make()
  ];
}
```
	Check FieldDefinition class to extend or use the methods there.

- authorize

	Define the options that can be used on actions (View/Delete/Edit)

Example
``` php
public function authorize()
{
  $this->delete = false;
  $this->edit = true;
  $this->view = true;
}
```

##### Views

Customize the _form.blade.php of each model, to match the model fields using the blade components included

###### Use 
``` php
@component('gridcomponents::input', [
	'name' => 'fieldname'    
])
```

- gridcomponents::input

	Is a regular input type
    ###### Properties
    	- name (name and id) - required
    	- caption, (title for the label)- default: ucfirst name
    	- value - default: emtpy
    	- type (input type) - default: text
    	- placeholder - default: empty

- gridcomponents::link

	Is a regular link tag
    ###### Properties
    	- location - required
    	- size, (xs,sm,md,lg)- default: md
    	- caption - default: emtpy
    	- icon (font-awesome) - default: close
    	- color (primary, default, danger, warning, info ..) - default: default
    	- id - default: empty
	
- gridcomponents::button

	Is a regular button
    ###### Properties
    	- type - button
    	- size, (xs,sm,md,lg)- default: md
    	- caption - default: emtpy
    	- icon (font-awesome) - default: close
    	- color (primary, default, danger, warning, info ..) - default: default
    	- id - default: empty

- gridcomponents::select

	Is a regular select tag
    ###### Properties
    	- name (name and id) - required
    	- caption, (title for the label)- default: ucfirst name
    	- options - required array of options
    	- placeholder - default: empty

##### Request

Populate your validation on rules on the FormRequest rules property

Example

``` php
namespace App\Http\Requests;

class ModelFormRequest extends BaseFormRequest
{
  /*
   * .....
   */
  protected $rules = [
    'name'  => 'required|min:4'
  ];
 
}


```

## Configuration

The configuration file named vueresourcegrid.php 

###### Options
- namespace, 

	Namespaces for controllers, models, requests and grids.
    

- views

	defines where to dump views


## Security

If you discover any security related issues, please email josue.haros@gmail.com instead of using the issue tracker.

## Credits

- Josue Haros

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.